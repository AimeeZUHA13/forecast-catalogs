## Forecast Catalog

## Propuesta de solución:
Decidí utilizar la ya conocida técnica de SCDs (Slowly Changing Dimensions) que se aplica normalmente a los Data Warehouse, dos de las razones más importantes por dicha elección es que:
1. Se ha solicitado un versioning de cada una de las tables, esto se traduce en un SCDs donde existen columnas que indican las fechas de inactividad, actividad o "activo desde" a "activo hasta" donde después se utilizan dichas columnas para construir vistas actuales e históricas de los datos.

2. Si en un futuro se desea realizar la extracción de más datos relacionados a atributos similares para los modelos predictivos, es más facil de una vez crear las tablas dimensionales que podrían conformar en un futuro un DWH para dicho proyecto.


### Puntos fuertes:
1. En caso de en un futuro crecer el proyecto, ya se tienen la base(DWH) para agregar más dimensiones/hechos.
2. Al orquestrarse por medio de Airflow, puede crecer hasta soportar toda la operación que un DWH necesite.

### Puntos débiles:
1. Es posible que algunos Data Engineers no tengan el conocimiento suficiente para trabajar un DWH.
2. Es posible que el DWH sea más complejo para las tareas que se llegan a solicitar o las que inclusive no se lleguen a.

### Preguntas adicionales

1. ¿Qué mejoras propondrías a tu solución para siguientes versiones?

    * Mejorar la conectividad con la Base de Datos, en este momento se realizan los queries con raw text, es mejor implementarlo con un ORM para más seguridad, transparencia, mejores prácticas, etc.
    * Mejorar la manipulación de las variables (ejem. credenciales), por medio de algún archivo config o YML.
    * Mejorar el docker-compose, ahorita es una configuración básica.

2. Tu solución le ha gustado al equipo y deciden incorporar otros procesos, habrá nuevas personas colaborando contigo,  ¿Qué aspectos o herramientas considerarías para escalar, organizar y automatizar tu solución?

    * Airflow por el momento puede ser soportado dentro de una máquina virtual (ejemp. AWS EC2) pero si se escala se necesitaría que tal vez estuviera bajo algún proveedor en la nube para evitar mantenimientos, failures.
    * Tener squads de Data Engineers enfocados a una dimension/fact en específico.
    * Colaborar muy de la mano con los Data Scients y Data Analyst.
    * Tratar que el almacenamiento de la información tanto raw como procesada sea en algún proveedor en la nube asi como de igual manera la Base de Datos.
    * Tener un linaje de datos.
    * Implementación de reglas de Calidad de Datos/Data Quality Management.
    * Realizar prubas unitarias y funcionales.
    * Documentar.
    * Implementar monitoreo.
    * Mejorar la parte de logs (crear una librería tal vez).

## Nota

> Renombrar la fecha de las carpetas de *data_municipios* al día que se ejecute el proceso para simular la extracción de datos diaria.

## Instrucciones Airflow
### Iniciar
```
docker-compose -f docker-compose.yml up --build
```
### Parar
```
docker-compose down
```

### Enter airflow container
```
docker exec -it {container_id} /bin/bash
```

### Dentro del airflow container:

#### Listar Dags
```
airflow dags list
```
#### Test dag
```
airflow dags test {dag_id} {execution_date_or_run_id, ex: 2022-01-01}
```
#### Listar dag tasks
```
airflow tasks list {dag_id}
```
### Test dag task
```
airflow dags test {dag_id} {task_id} {execution_date_or_run_id, ex: 2022-01-01}