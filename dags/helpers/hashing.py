import hashlib


def get_hash(string):
    hash_object = hashlib.sha1(string.encode("utf-8"))
    hex_dig = hash_object.hexdigest()
    return hex_dig
