query_schema_stg = "CREATE SCHEMA IF NOT EXISTS staging;"

query_schema_dim = "CREATE SCHEMA IF NOT EXISTS dimension;"

query_avg_table = """
    CREATE TABLE IF NOT EXISTS staging.forecast_avg(
        ides INTEGER,
        idmun INTEGER,
        nes VARCHAR,
        nmun VARCHAR,
        tmax_avg INTEGER,
        prec_avg INTEGER,
        extracted_at VARCHAR
    );
"""

query_get_avg_table = "SELECT * FROM staging.forecast_avg"

query_stg_table = """
    CREATE TABLE IF NOT EXISTS staging.forecast(
        ides INTEGER,
        idmun INTEGER,
        nes VARCHAR,
        nmun VARCHAR,
        tmax_avg INTEGER,
        prec_avg INTEGER,
        value INTEGER,
        extracted_at VARCHAR,
        surrogate_key VARCHAR,
        PRIMARY KEY (ides,idmun,surrogate_key)
    );
"""

query_dim_table = """
    CREATE TABLE IF NOT EXISTS dimension.forecast(
        ides INTEGER,
        idmun INTEGER,
        nes VARCHAR,
        nmun VARCHAR,
        tmax_avg INTEGER,
        prec_avg INTEGER,
        value INTEGER,
        extracted_at VARCHAR,
        surrogate_key VARCHAR,
        PRIMARY KEY (ides,idmun,surrogate_key)
    );
"""

query_dim_hist_table = """
    CREATE TABLE IF NOT EXISTS dimension.forecast_hist(
        ides INTEGER,
        idmun INTEGER,
        nes VARCHAR,
        nmun VARCHAR,
        tmax_avg INTEGER,
        prec_avg INTEGER,
        value INTEGER,
        surrogate_key VARCHAR,
        start_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
        end_date TIMESTAMP WITHOUT TIME ZONE,
        is_active BOOLEAN NOT NULL DEFAULT true,
        PRIMARY KEY (ides,idmun,surrogate_key)
    );
"""


update_dim_hist = """
    UPDATE dimension.forecast_hist 
      SET 
          is_active = false,
          end_date = NOW()
      FROM 
      (
          SELECT 
          sfo.ides, 
          sfo.idmun, 
          sfo.surrogate_key 
          FROM 
              staging.forecast sfo
          LEFT JOIN dimension.forecast_hist forh ON forh.ides = sfo.ides 
              AND forh.idmun = sfo.idmun 
              AND forh.surrogate_key = sfo.surrogate_key 
          WHERE 
              forh.surrogate_key IS NULL
      ) tmp
      WHERE 
          dimension.forecast_hist.ides = tmp.ides 
          AND dimension.forecast_hist.idmun = tmp.idmun 
          AND dimension.forecast_hist.end_date IS NULL;
"""

insert_dim_hist = """
    INSERT INTO dimension.forecast_hist 
      SELECT 
        sfo.ides,
        sfo.idmun,
        sfo.nes,
        sfo.nmun,
        sfo.tmax_avg,
        sfo.prec_avg,
        sfo.value,
        sfo.surrogate_key,
        NOW() AS start_date, 
        NULL AS end_date
      FROM 
          staging.forecast sfo
      LEFT JOIN dimension.forecast_hist forh ON forh.ides = sfo.ides 
          AND forh.idmun = sfo.idmun 
          AND forh.surrogate_key = sfo.surrogate_key 
      WHERE 
          forh.surrogate_key IS NULL;
"""

delete_dim = """
    DELETE FROM 
          dimension.forecast 
      WHERE 
          surrogate_key IN (
              SELECT 
                  surrogate_key 
              FROM 
                  staging.forecast
          );
"""

insert_dim = """
    INSERT INTO dimension.forecast (
        ides, idmun, nes, nmun,
        tmax_avg, prec_avg, value,
        extracted_at, surrogate_key
      ) 
      SELECT 
        ides,
        idmun,
        nes,
        nmun,
        tmax_avg,
        prec_avg,
        value,
        extracted_at,
        surrogate_key
      FROM 
        staging.forecast;
"""

truncate_stg = "TRUNCATE TABLE staging.forecast;"
truncate_stg_avg = "TRUNCATE TABLE staging.forecast_avg;"
