import glob
import json
import gzip
import os
import pytz
import sys
import datetime as dt
import pandas as pd
from helpers.logging import get_logger
from helpers.hashing import get_hash
from helpers.database_connection import get_engine
from helpers.queries import query_get_avg_table

sys.path.insert(0, "/usr/local/airflow/dags")

logger = get_logger()
today = dt.datetime.now(pytz.timezone("America/Mexico_City")).strftime("%Y%m%d")
time = dt.datetime.now().strftime("%H%M%S")
credentials = {
    "host": "forecast-catalogs-postgres-1",
    "user": "airflow",
    "password": "airflow",
    "database": "airflow",
    "port": "5432",
}


def process_data():
    logger.info("Processing webservice data")
    forecast_data = read_file()
    for dict in forecast_data:
        path = f"dags/resources/raw/{dict['nes']}/{today}/{time}/"
        exist = os.path.exists(path)
        if not exist:
            os.makedirs(path)
            df = pd.DataFrame([dict])
            df = calculate_avg_data(df)
            logger.info("Loading data into the table forecast_avg.")
            df.to_csv(f"{path}/forecast_avg.csv", index=False)
            insert_avg_data(df)
        else:
            logger.info(f"Directory for {dict['nes']} already exist.")


def read_file():
    with gzip.open("DailyForecast_MX", "rt", encoding="UTF-8") as zipfile:
        forecast_data = json.load(zipfile)
        return forecast_data


def cross_data():
    df_ref = pd.DataFrame()

    path_refined = "dags/resources/refined/*/*/*.csv"
    files_refined = glob.glob(path_refined)

    logger.info("Crossing data_municipios & data_webservice")
    for file_refined in files_refined:
        date_refined = file_refined.split("/")[4]
        if today == date_refined:
            logger.info(f"Processing {file_refined} file.")
            df_ref = pd.read_csv(file_refined)

    df_raw = get_avg_data()

    df_ref.rename(
        columns={"Cve_Ent": "ides", "Cve_Mun": "idmun", "Value": "value"}, inplace=True
    )

    columns_cast = {
        "ides": "Int64",
        "idmun": "Int64",
        "nes": "string",
        "nmun": "string",
        "extracted_at": "string",
    }
    df_raw = df_raw.astype(columns_cast)

    df_merged = pd.merge(
        left=df_raw, right=df_ref, left_on=["ides", "idmun"], right_on=["ides", "idmun"]
    )

    df_merged["extracted_at"] = today
    df_merged = hash_columns(df_merged)

    logger.info("Loading data into the table forecast.")
    path_refined = f"dags/resources/refined/data_webservice/{today}"
    exist = os.path.exists(path_refined)
    if not exist:
        os.makedirs(path_refined)
    df_merged.to_csv(f"{path_refined}/forecast.csv", index=False)
    insert_merged_data(df_merged)


def get_avg_data():
    engine = get_engine(**credentials)

    df = pd.read_sql(sql=query_get_avg_table, con=engine)

    return df


def calculate_avg_data(df):
    df_avg = pd.DataFrame()

    df_avg["ides"] = df["ides"].values
    df_avg["idmun"] = df["idmun"].values
    df_avg["nes"] = df["nes"].values
    df_avg["nmun"] = df["nmun"].values
    df_avg["tmax_avg"] = df["tmax"].mean()
    df_avg["prec_avg"] = df["prec"].mean()
    df_avg["extracted_at"] = today

    return df_avg


def hash_columns(df):
    columns_surrogate = ["ides", "idmun", "nes", "nmun", "value"]

    df["surrogate_key"] = df[columns_surrogate].apply(
        lambda row: "".join(row.values.astype(str)), axis=1
    )
    df["surrogate_key"] = df["surrogate_key"].apply(get_hash)

    return df


def insert_avg_data(df):
    engine = get_engine(**credentials)

    df.to_sql(
        "forecast_avg",
        engine,
        schema="staging",
        if_exists="append",
        index=False,
        method="multi",
        chunksize=200,
    )


def insert_merged_data(df):
    engine = get_engine(**credentials)

    df.to_sql(
        "forecast",
        engine,
        schema="staging",
        if_exists="append",
        index=False,
        method="multi",
        chunksize=200,
    )
