import sys
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.operators.dummy import DummyOperator
from libs.database_handler import db_handler, load_dwh
from libs.data_handler import process_data, cross_data
from libs.webservice import webservice_handler

sys.path.insert(0, "/usr/local/airflow/dags")

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2023, 3, 16),
    "email": ["aimee.@company.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5),
}

with DAG(
    "dag_forecast",
    default_args=default_args,
    catchup=False,
    schedule_interval=timedelta(hours=5),
) as dag:
    start = DummyOperator(task_id="start_task")

    with TaskGroup(group_id=f"raw_layer") as raw_layer:
        create_db_objects = PythonOperator(
            task_id="create_db_objects",
            python_callable=db_handler,
            dag=dag,
        )

        connect_webservice = PythonOperator(
            task_id="connect_webservice",
            python_callable=webservice_handler,
            dag=dag,
        )

        process_webservice_data = PythonOperator(
            task_id="process_data",
            python_callable=process_data,
            dag=dag,
        )

        (create_db_objects >> connect_webservice >> process_webservice_data)

    refined_layer = PythonOperator(
        task_id="cross_data",
        python_callable=cross_data,
        dag=dag,
    )

    load_dwh_data = PythonOperator(
        task_id="load_task",
        python_callable=load_dwh,
        dag=dag,
    )

    start >> raw_layer >> refined_layer >> load_dwh_data
